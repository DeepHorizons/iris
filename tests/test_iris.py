import unittest
import json
from context import app
import warnings
import pprint

class BaseTest(unittest.TestCase):
    """
    Provides proper setup and teardown for all tests
    """

    def setUp(self):
        config_object = app.Config.get_config_object()
        app.app.config['config'] = config_object
        app.app.config['TESTING'] = True

        self.app = app.app.test_client()

    def tearDown(self):
        pass


class RootTest(BaseTest):
    def test_root_page(self):
        print("Testing /api...")
        rv = self.app.get('/api/')
        data = json.loads(rv.data.decode())
        assert 'api_versions' in data
        assert 'urls' in data
        assert '/api/evaluate' in data['urls']
        print("done!")

class EvaluateTest(BaseTest):
    def test_evaluate_get(self):
        print(".Testing /api/evaluate get...")
        rv = self.app.get('/api/evaluate')
        data = json.loads(rv.data.decode())

        assert 'plugins' in data
        assert 'grammars' in data

        assert type(data['plugins']) is dict
        for plugin in data['plugins']:
            assert type(plugin) is str
            assert type(data['plugins'][plugin]) is dict
            assert type(data['plugins'][plugin]['description']) is str
            assert type(data['plugins'][plugin]['grammars']) is list
            for grammar in data['plugins'][plugin]['grammars']:
                assert type(grammar) is str

        for grammar in data['grammars']:
            assert type(grammar) is str
        print("done!")

    def test_evaluate_post_no_data(self):
        print("Testing /api/evaluate post with no data...")
        rv = self.app.post('/api/evaluate')
        assert rv.status_code == 400
        data = json.loads(rv.data.decode())
        assert 'message' in data
        assert 'No JSON' in data['message']
        print("done!")

    def test_evaluate_post_no_string(self):
        print("Testing /api/evaluate post with no string...")
        rv = self.app.post('/api/evaluate', data=dict(fail="5"))
        assert rv.status_code == 400
        data = json.loads(rv.data.decode())
        assert 'message' in data
        assert 'missing' in data['message']
        print("done!")

    def test_evaluate_post_1(self):
        print("Testing /api/evaluate post...")
        rv = self.app.post('/api/evaluate', data=dict(
            string="testing a word word word")
            )
        assert rv.status_code == 200
        data = json.loads(rv.data.decode())

        assert 'string' in data
        assert data['string'] == str(('a','word word word'))

    def test_evaluate_post_2(self):
        rv = self.app.post('/api/evaluate', data=dict(
            string="testing c word word word")
            )
        assert rv.status_code == 200
        data = json.loads(rv.data.decode())

        assert 'string' in data
        assert 'No match' in data['string']

    def test_evaluate_post_3(self):
        
        rv = self.app.post('/api/evaluate', data=dict(
            string="completely different string")
            )
        assert rv.status_code == 200
        data = json.loads(rv.data.decode())

        assert 'string' in data
        assert 'No match' in data['string']

    def test_evaluate_post_4(self):
        rv = self.app.post('/api/evaluate', data=dict(
            string="tfsting a word word word")
            )
        assert rv.status_code == 200
        data = json.loads(rv.data.decode())

        assert 'string' in data
        assert data['string'] == str(('a','word word word'))

    def test_evaluate_post_5(self):
        rv = self.app.post('/api/evaluate', data=dict(
            string="tsting a word word word")
            )
        assert rv.status_code == 200
        data = json.loads(rv.data.decode())

        assert 'string' in data
        assert data['string'] == str(('a','word word word'))

    def test_evaluate_post_6(self):
        rv = self.app.post('/api/evaluate', data=dict(
            string="tefsting a word word word")
            )
        assert rv.status_code == 200
        data = json.loads(rv.data.decode())

        assert 'string' in data
        assert data['string'] == str(('a','word word word'))

    def test_evaluate_post_7(self):
        rv = self.app.post('/api/evaluate', data=dict(
            string="tfsftng a word word word")
            )
        assert rv.status_code == 200
        data = json.loads(rv.data.decode())

        assert 'string' in data
        assert data['string'] == str(('a','word word word'))

    def test_evaluate_post_8(self):
        rv = self.app.post('/api/evaluate', data=dict(
            string="ftfsftfg a word word word")
            )
        assert rv.status_code == 200
        data = json.loads(rv.data.decode())

        assert 'string' in data
        assert 'No match' in data['string']
        print("\ndone!")


if __name__ == '__main__':
    unittest.main()
