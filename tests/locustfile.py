from locust import HttpLocust, TaskSet, task


class UserBehavior(TaskSet):
    @task
    def lights_on(self):
        data = {'string': "turn on the lights in the kitchen"}
        self.client.post('/api/evaluate', data=data)

    @task
    def lights_off(self):
        data = {'string': "turn off the lights in the kitchen"}
        self.client.post('/api/evaluate', data=data)

    @task
    def weather(self):
        data = {'string': "what is the weather in rochester"}
        self.client.post('/api/evaluate', data=data)
    
    @task
    def weather_ny(self):
        data = {'string': "what is the weather in new york"}
        self.client.post('/api/evaluate', data=data)


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 5000
    max_wait = 9000
