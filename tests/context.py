"""
Provides the correct context to get the app

From http://docs.python-guide.org/en/latest/writing/structure/
"""
# Dont lint this file
# flake8: noqa
import os.path as path
import sys
from os import chdir

APP_LOCATION = path.abspath(path.join(path.dirname(__file__), '..', 'iris'))
sys.path.insert(0, APP_LOCATION)  # Add the root

chdir('../iris/')

import app  # app.py
