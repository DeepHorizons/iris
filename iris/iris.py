import os, time
from plugins import *
from flask import Blueprint, current_app, jsonify, request
from flask_restful import Api, Resource, abort
from traitlets import Integer, Bool
from common.api.flask import Root, ConfigurableResource, get_body, get_required_field, parse_value
#from common.config import Config
import pprint

API_VERSIONS = [0.3]
IRIS_VERSION = 0.3

api_blueprint = Blueprint('api', __name__)
api = Api(api_blueprint)
Root.api_bluprint_name = api_blueprint.name
Root.api_versions = API_VERSIONS
Root.frame = globals()
api.add_resource(Root, '/')

@api.resource('/evaluate')
class Evaluate(ConfigurableResource):
    description = "Determine if given string matches a loaded plugin grammar."
    editdistance = Integer(10000000,help="Maximum editdistance").tag(config=True)
    debug = Bool(False,help="Enable Debug Mode.").tag(config=True)
    def get(self):
        data = {'plugins':{},
                'grammars':[],
                'regex':[]}
        for pluginName,plugin in plugins.items():
            try:
                grammars = list(plugin.plugin.grammars.keys())
            except AttributeError as e:
                print(f"Malformed plugin {pluginName}, skipping...")
                continue
            data['plugins'][pluginName] = {}
            data['plugins'][pluginName]['description'] = plugin.description
            data['plugins'][pluginName]['grammars'] = grammars
            data['grammars'] += grammars
            data['regex'] += [plugin.plugin.regex[grammar] for grammar in grammars]
        # jsonify sorts things alphabetically,
        # We do not want this because it ignores the order of OrderedDicts
        current_app.config['JSON_SORT_KEYS'] = False
        resp = jsonify(data)
        current_app.config['JSON_SORT_KEYS'] = True
        return resp

    def post(self):
        body = get_body()
        string = get_required_field(body, 'string')
        print(f"Matching '{string}'...")
        match = {'editdistance':self.editdistance,'length':0}
        for pluginName,plugin in plugins.items():
            if(self.debug): print("Plugin Name:",pluginName)
            try:
                tmp = plugin.plugin.match(self.editdistance,string)
            except AttributeError as e:
                print(f"Error reading plugin {pluginName}, skipping...")
                continue
            if(tmp):
                if( (tmp['editdistance'] < match['editdistance']) or 
                    (tmp['editdistance'] == match['editdistance'] and tmp['length'] > match['length']) ):
                    match = tmp
        if(match['length'] == 0): #abort(400, message="No match found for string" + string)
            resp = {'string':"No match found for string: " + string,
                    'match':False}
        else:
            resp = match['plugin'].grammars[match['grammar']](*match['args'])
            resp['match'] = True
        if(self.debug):
            print("Returing:",end="")
            pprint.pprint(resp)
        return jsonify(resp)