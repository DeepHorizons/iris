import regex as re
from traitlets.config import Configurable
from traitlets import Integer
from copy import deepcopy

# The following levenshtein implementation was used from wikipedia. Here is the credit from the page, located:
# url: https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Python
# it is the '6th Version'
# Christopher P. Matthews
# christophermatthews1985@gmail.com
# Sacramento, CA, USA

def levenshtein(s, t):
        ''' From Wikipedia article; Iterative with two matrix rows. '''
        if s == t: return 0
        elif len(s) == 0: return len(t)
        elif len(t) == 0: return len(s)
        v0 = [None] * (len(t) + 1)
        v1 = [None] * (len(t) + 1)
        for i in range(len(v0)):
            v0[i] = i
        for i in range(len(s)):
            v1[0] = i + 1
            for j in range(len(t)):
                cost = 0 if s[i] == t[j] else 1
                v1[j + 1] = min(v1[j] + 1, v0[j + 1] + 1, v0[j] + cost)
            for j in range(len(v0)):
                v0[j] = v1[j]
        return v1[len(t)]

class Plugin(Configurable):
    errors = Integer(3,help="Maximum total allowable errors").tag(config=True)
    substitutions = Integer(1,help="Maximum total allowable substitutions").tag(config=True)
    deletions = Integer(1,help="Maximum total allowable deletions").tag(config=True)
    insertions = Integer(1,help="Maximum total allowable insertions").tag(config=True)
    def __init__(self, grammars, errors=0, substitutions=0, insertions=0, deletions=0):
        self.grammars = grammars
        self.initialized = False
        self.errorStrs = []
        if(errors != 0): 
            self.errors = errors
            self.errorStrs.append("e<=" + str(errors))
        if(substitutions != 0): 
            self.substitutions = substitutions
            self.errorStrs.append("s<=" + str(substitutions))
        if(insertions != 0): 
            self.insertions = insertions
            self.errorStrs.append("i<=" + str(insertions))
        if(deletions != 0): 
            self.deletions = deletions
            self.errorStrs.append("d<=" + str(deletions))
        # Dictionary to store the regex versions of the given grammars
        self.regex = {}
        for grammar in self.grammars:
            self.grammars = grammars
            # Dictionary to store the regex versions of the given grammars
            self.regex = {}
            self.regexChoices = {}
            for grammar in self.grammars:
                self.regex[grammar] = ''
                grammarArray = [x for x in re.split('{|}',grammar) if x != '']
                keywordRun = False
                # Time to create a regex statment
                for i,item in enumerate(grammarArray): 
                    # this is a wildcard
                    if('*' in item): 
                        if(i == len(grammarArray)-1): 
                            self.regex[grammar] += '(?<= )(.*)'
                        elif(i == 0):
                            self.regex[grammar] += '(.*?)'
                        else: 
                            self.regex[grammar] += '(?<= )(.*?)'
                    # this is a multiword
                    elif('|' in item):
                        for j,choice in enumerate(item.split('|')):
                            if(j == 0): 
                                if(i != 0):
                                    self.regex[grammar] += '(?<= )('
                                else:
                                    self.regex[grammar] += '('
                            else: 
                                self.regex[grammar] += '|'
                            self.regex[grammar] += choice
                        #self.regex[grammar] += ')(?= )'  # TODO XXX Was the space causing an issue or was it needed?
                        self.regex[grammar] += ')(?=)'
                    else:
                        self.regex[grammar] += item
                self.regex[grammar] += '$'
                
    def loadConfig(self, config_object):
        if(len(self.errorStrs) == 0):
            self.config.merge(config_object)
            if(self.errors != 0): 
                self.errorStrs.append("e<=" + str(self.errors))
            if(self.substitutions != 0): 
                self.errorStrs.append("s<=" + str(self.substitutions))
            if(self.insertions != 0): 
                self.errorStrs.append("i<=" + str(self.insertions))
            if(self.deletions != 0): 
                self.errorStrs.append("d<=" + str(self.deletions))


        if(len(self.errorStrs) > 0):
            for grammar in self.regex:
                self.regex[grammar] = "(?e)(" + self.regex[grammar] + '){'
                for e in self.errorStrs:
                    if(self.errorStrs.index(e) != 0): self.regex[grammar] += ','
                    self.regex[grammar] += e
                self.regex[grammar] += "}"

    def match(self, editdistance, string):
        match = {'editdistance':editdistance,'length':0}
        for grammar in self.regex:
            editDist = levenshtein(string,grammar)
            if(editDist < match['editdistance'] or (editDist == match['editdistance'] and len(grammar) > match['length'])):
                regexReturn = re.fullmatch(self.regex[grammar], string, re.IGNORECASE)
                if(regexReturn):
                    match = {'plugin':self,
                             'grammar':grammar,
                             'args':regexReturn.groups()[1:],
                             'editdistance':editDist,
                             'length':len(grammar),
                             'string':string,
                             'match':True}
        if(match['length'] == 0): return False
        else: return match
