from plugins.plugin import *

def intro(*args):
    string = "Hello. I am the {} Home Automation Framework. I am a collection from several services designed and built by Evan, Ian, Joe, and Joshua. The goal was to make building a smart home easy and make it readily available to the hobbyist."
    return {'say': string.format('Cicladis'), 'string': string.format('Cyclades')}

description = "Talks about Cyclades"
plugin = Plugin({
                    "introduce yourself": intro,
                })
