from plugins.plugin import *


def echo(*args):
    string = ""
    for arg in args:
        string += arg
    return {'string':string, 'say': string}

description = "Echos whatever was sent"
plugin = Plugin({
                     "echo {*}":echo,
                },
                )
