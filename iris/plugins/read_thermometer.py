from plugins.plugin import *
import requests
import sys
arke_url = 'https://steel.student.rit.edu:5013'

# r = requests.post('http://httpbin.org/post', data = {'key':'value'})
# r = requests.put('http://httpbin.org/put', data = {'key':'value'})
# r = requests.delete('http://httpbin.org/delete')
# r = requests.head('http://httpbin.org/get')
# r = requests.options('http://httpbin.org/get')

# HANDLES READING DEVICES WITH A SPECIFIED NAME/LOCATION
def thermometer(*args):
    #device = args[0]
    location = args[0]

    tags = set()
    names = set()
    device_dict = {}
	# get list of locations from arke
    r = requests.get(arke_url + '/api/node')
    nd = eval(r.text)
    nodes = nd['nodes'] # list of dictionaries

    # check to see if the GET worked
    if(r.status_code == 200):
        for n in nodes:
            try:
                t = n['tag']
                tags = tags | {t.lower()}
            except:
                pass
            try:
                name = n['name']
                names = names | {name.lower()}
                if("thermometer" in name.lower()):
                    device_dict = n
            except:
                pass

    	# check to see if location is valid
        if(location.lower() in tags):
            # check to see if this location has a thermometer
            if("thermometer" in names):
                # device_dict must be valid
                r2 = requests.get(arke_url + '/api/node/' + device_dict['id'])
                if(r2.status_code == 200 ):
                    say = 'The temperature is ' + r2.text
                    return {'string':"The temperature in " + location + ": " + r2.text, 'say': say}
                else:
                    return {'string':"Error: Arke responded with " + str(r2.status_code)}
            else:
                return {'string': 'No thermometer in ' + location}
        else:
            return {'string':"Invalid location: " + location}

    else:
        return {'string':'ARKE did not respond with a list of nodes.'}


description = "Reads thermometers throughout the house."
plugin = Plugin({
                     "What temperature is it in {*}":thermometer,
                     "What is the temperature in {*}":thermometer,
                },
                errors=6,
                )
