from plugins.plugin import *

def goodnight(*args):
    return {'say': 'goodnight'}

description = "Says goodnight"
plugin = Plugin({
                    "goodnight": goodnight,
                    "good night": goodnight,
                })
