# -*- coding: utf-8 -*-
try:
    from plugins.plugin import *
except ImportError as e:
    # Local import
    from plugin import *
import os
import pathlib
import base64
import random

MUSIC_DIR = '~/Music'
MUSIC = {}

MUSIC_DIR = os.path.expanduser(MUSIC_DIR)
MUSIC_DIR = os.path.expandvars(MUSIC_DIR)

for obj in pathlib.Path(MUSIC_DIR).rglob("*"):
    if obj.is_file():
        path = str(obj)
        MUSIC[path] = obj


def play(*args):
    if len(args) == 1:
        name = args[0]
        artist = None
    elif len(args) == 2:
        name = args[0]
        artist = args[1]
    
    # TODO search by ID3 tags
    # TODO search by filename
    global MUSIC
    m = []
    for filename in MUSIC:
        if name in filename.lower().replace("-", " "):
            m.append(MUSIC[filename])

    if len(m) == 0:
        return {"string": "Not found"}
    else:
        audio = random.choice(m)
        data = audio.read_bytes()
        data = base64.b64encode(data)
        data = data.decode('ascii')
        return {"string": audio.name, "play": data}

    


description = "Play music"
plugin = Plugin({
                    "play {*}": play,
                    #"play {*} by {*}": play,
                },)

if __name__ == '__main__':
    play("abhi the nomad")