# -*- coding: utf-8 -*-
from plugins.plugin import *
#import pywapi
import python_weather
import asyncio
import datetime

UNITS = 'imperial'

def weather_here(*args):
    # TODO to a geoip lookup
    return weather(None,'Dover, NH')

async def get_weather_data(location):
    async with python_weather.Client(unit=python_weather.IMPERIAL) as client:
        weather = await client.get(location)
    return weather

def weather(*args):
    location = args[1]

    # Get the location ID (Just grab the first one)
    weather = asyncio.run(get_weather_data(location))
    location = weather.nearest_area.name
    forcasts = [f for f in weather.forecasts]
    today = forcasts[0]
    #units = weather['units']
    condition = weather.current.description.lower()

    temp = weather.current.temperature
    maxTemp = f"{today.highest_temperature}°"
    minTemp = f"{today.lowest_temperature}°"
    
    message = "In {} it is {} with a high of {} and a low of {}.".format(location, temp, maxTemp, minTemp)
    say = "It is {} with a temperature of {}.".format(condition, temp)

    return {'string': message, 'say': say, 'weather': condition}

def what_time(*args):
    dt = datetime.datetime.now()
    msg = f"It is {dt.strftime('%H:%M')}"
    return {'string': msg, 'say': msg}



description = "Uses pywapi and weather.com api to get temperature in requested location."
plugin = Plugin({
                    "{What is|Whats} the weather": weather_here,
                    "{What is|Whats} the weather in {*}":weather,
                    "{what's|what is|whats} the weather like in": weather,
                    "What time is it":what_time,
                    "whats the time":what_time,
                })
