from plugins.plugin import *
import requests
import sys
import json

arke_url = 'https://steel.student.rit.edu:5013'

# r = requests.post('http://httpbin.org/post', data = {'key':'value'})
# r = requests.put('http://httpbin.org/put', data = {'key':'value'})
# r = requests.delete('http://httpbin.org/delete')
# r = requests.head('http://httpbin.org/get')
# r = requests.options('http://httpbin.org/get')

# HANDLES TURNING ON/OFF DEVICES WITH A SPECIFIED LOCATION
def device(*args):
    command = args[0]
    device = args[1]
    location = args[2]
    dt = "-1";

    if(command == "on"):
        dt = "1"
    else:
        dt = "0"

    tags = set()
    names = set()
    device_dict = {}

	# get list of locations from arke
    r = requests.get(arke_url + '/api/node')
    nd = eval(r.text)
    nodes = nd['nodes'] # list of dictionaries

    # check to see if the GET worked
    if(r.status_code == 200):
        for n in nodes:
            try:
                t = n['tag']
                tags = tags | {t.lower()}
            except:
                pass
            try:
                name = n['name']
                names = names | {name.lower()}
                if(device.lower() == name.lower()):
                    device_dict = n
            except:
                pass

    	# check to see if location is valid
        if(location.lower() in tags):
            if(device.lower() in names):
                r2 = requests.put(arke_url + '/api/node/' + device_dict['id'], json={'data':dt})
                if(r2.status_code == 200 | r2.status_code == 204):
                    say = 'Ok'
                    return {'string':"Successfully turned " + command + " the " + device + " in " + location, 'say': say}
                else:
                    return {'string':"Error: Arke responded with " + str(r2.status_code)}
            else:
                say = "I'm sorry, I don't see that device"
                return {'string':"Invalid device: " + device, 'say': say}
        else:
            say = "I'm sorry, I don't know that location"
            return {'string':"Invalid location: " + location, 'say': say}
    else:
        return {'string':'ARKE did not respond with a list of nodes.'}


description = "Controls devices throughout the house."
plugin = Plugin({
                     "turn {on|off} the {*} in {*}":device,
                },
                errors=6,
                )
