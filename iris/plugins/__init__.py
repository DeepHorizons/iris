import os
import time
import importlib
import threading
from traitlets import Float, Unicode, List
from traitlets.config import Configurable
from .plugin import *  # noqa


# Thread to poll folder for file additions/removals/changes
class PluginLoaderThread(Configurable):
    plugin_path = Unicode(os.path.abspath(os.path.dirname(__file__)),
                          help="Where are the plugins located").tag(config=True)
    plugin_poll_rate = Float(5, help="How often should we poll to see if plugins have changed").tag(config=True)
    excluded_files = List(['__init__.py', 'plugin.py'], help="What files should be excluded").tag(config=True)

    def exit(self): self.running = False

    def begin(self):
        if(self.config_object is None):
            print("Error! Config file not set for plugin thread.")
            return
        else:
            previous = self.pollFolder()
            for pluginName in self.pollFolder():
                try:
                    plugins[pluginName].plugin.loadConfig(self.config_object)
                except AttributeError as e:
                    print(f"Bad plugin '{pluginName}', skipping...")
                    continue
            while self.running:
                current = self.pollFolder()
                for pluginName in current:
                    if(pluginName not in plugins):
                        plugins[pluginName] = importlib.import_module('plugins.' + pluginName)
                        plugins[pluginName].plugin.loadConfig(self.config_object)
                    elif(current[pluginName] > previous[pluginName]):
                        importlib.reload(plugins[pluginName])
                        plugins[pluginName].plugin.loadConfig(self.config_object)
                for pluginName in previous:
                    if(pluginName not in current):
                        plugins.pop(pluginName, None)
                previous = current
                time.sleep(self.plugin_poll_rate)

    def __init__(self):
        for pluginName in self.pollFolder():
            plugins[pluginName] = importlib.import_module('plugins.' + pluginName)
        self.running = True
        self.thread = threading.Thread(target=self.begin, daemon=True)
        self.config_object = None

    def pollFolder(self):
        """
        Returns a dictionary containing files in the plugin path as keys and update time as the value
        """
        # Get the functions so we don't write a huge one liner
        splitext = os.path.splitext
        getmtime = os.path.getmtime
        join = os.path.join

        plugins = os.listdir(self.plugin_path)
        plugins = [f for f in plugins if ('py' in splitext(f)[1] and f not in self.excluded_files)]
        folders = {splitext(f)[0]: getmtime(join(self.plugin_path, f)) for f in plugins}
        return folders


plugins = {}
pluginLoaderThread = PluginLoaderThread()
