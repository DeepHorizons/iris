from plugins.plugin import *
import requests
import sys
arke_url = 'https://steel.student.rit.edu:5013'

# r = requests.post('http://httpbin.org/post', data = {'key':'value'})
# r = requests.put('http://httpbin.org/put', data = {'key':'value'})
# r = requests.delete('http://httpbin.org/delete')
# r = requests.head('http://httpbin.org/get')
# r = requests.options('http://httpbin.org/get')

# HANDLES READING DEVICES WITH A SPECIFIED NAME/LOCATION
def is_it_on(*args):
    device = args[0]

    names = set()
    device_dict = {}
	# get list of locations from arke
    r = requests.get(arke_url + '/api/node')
    nd = eval(r.text)
    nodes = nd['nodes'] # list of dictionaries

    # check to see if the GET worked
    if(r.status_code == 200):
        for n in nodes:
            try:
                name = n['name']
                names = names | {name.lower()}
                if(name.lower() == device.lower()):
                    device_dict = n
            except:
                pass

        if( device.lower() in names): #
            # device_dict must be valid
            r2 = requests.get(arke_url + '/api/node/' + device_dict['id'])
            if("1" in r2.text):
                say = device + 'is on'
                return {"string": device + " is on. ", 'say': say}
            else:
                say = device + 'is off'
                return {"string": device + " is off. ", 'say': say}
        else:
            return {'string': 'Invalid device name: \"' + device + "\""}

    else:
        return {'string':'ARKE did not respond with a list of nodes.'}


description = "Determines if a device is on or off. "
plugin = Plugin({
                     "Is {*} on":is_it_on,
                },
                errors=6,
                )
