from plugins.plugin import *

def space(*args):
    string = "I'm sorry Dave. I'm afraid I can't do that."
    return {'say': string, 'string': string}

def forrest_gump(*args):
    string = "You never know what you're gonna get"
    return {'say': string, 'string': string}

description = "Movie quotes"
plugin = Plugin({
                    "open the pod bay doors": space,
                    "open the pod bay doors hal": space,
                    "{life is|life's} like a box of chocolates": forrest_gump,
                })
