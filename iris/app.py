#!/usr/bin/env python
import os, argparse
from flask import Flask
from flask_cors import CORS
from iris import api_blueprint,pluginLoaderThread
from ui import ui_blueprint
from common.config import ConfigApplication

conf = ConfigApplication()
conf.default_config_file_name = 'iris_config.py'

app = Flask(__name__)
CORS(app)
app.register_blueprint(api_blueprint, url_prefix='/api')
app.register_blueprint(ui_blueprint, url_prefix='/')

# If we are a main script, then see if we want a config generated
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--generate-config", help="Generate a config file", action="store_true")
    parser.add_argument("-f", "--file", help="File used for configuration")
    args = parser.parse_args()
    if args.generate_config:
        # args.file is None if not defined
        conf.write_config_file(args.file)
        exit(0)
else:
    args = None
 
# Get the config file, set up stuff
environ = os.environ['CONFIG'] if ('CONFIG' in os.environ) else None
file = args.file if (args and args.file) else environ
config_object = conf.load_config_file(file)
app.config['config'] = config_object
pluginLoaderThread.config_object = config_object
pluginLoaderThread.thread.start()

if __name__ == '__main__': app.run(debug=True,host='0.0.0.0',port=5001)
