Iris Plugin System - A Cyclades Module
====
_An NLP **platform** - batteries provided by you!_

A plugin system which allows mapping of strings to actions (function calls) through a flask RESTful api.

Authored By: Joseph Gambino, Joshua Milas, Evan McDonough, Ian Doten

Requires python3.6

To install:
1. Use this command to pull the common cyclades package.
	git clone git@kgcoe-git.rit.edu:Cyclades/common
2. Make a virtualenv for the project and source it with the following
	pip install virtualenv
	virtualenv -p <path to your python3.6 install> <name of your virtualenv>
	source <path to your virtualenv>/bin/activate
	note: use "deactivate" to exit your virtual environment. You should use a separate virtual environment for each project you work on.	
3. Run the following command to install the required packages.
	pip install common/
	pip install -r requirements.txt

To run:
python app.py