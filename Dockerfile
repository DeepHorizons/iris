FROM cyclades/images:common

# Install app requirements
COPY requirements.txt /srv/requirements.txt
RUN cd /srv && pip3 install -r requirements.txt

# Copy the application
COPY iris /srv/iris
ENV FLASK_APP /srv/iris/app.py

# Setup UWSGI
COPY uwsgi.ini /srv/uwsgi.ini

EXPOSE 5000

RUN cd /srv/iris/
CMD ["flask", "run", "--host=0.0.0.0", "--with-threads"]
